
//APP_USR-7ca725bf-fac6-4ca3-a0aa-9dd6744873ce
//TEST-4981d611-039b-4be5-bf4d-7299fe0d7d96
const mp = new MercadoPago('APP_USR-7ca725bf-fac6-4ca3-a0aa-9dd6744873ce', {
    locale: 'es-AR'
});


const apimp = {
    checkout: (config) => {

        if (config.amount) {
            var r = btoa((config.redirect) ? config.redirect : window.location.hostname + '/payment.html'),
                e = btoa((config.email) ? config.email : 'info@newhorizons.uy'),
                a = btoa(config.amount),
                d = btoa((config.description) ? config.description : 'Compra Online'),
                extid = (config.extid) ? config.extid : false;

            mp.checkout({
                tokenizer: {
                    totalAmount: config.amount,
                    backUrl: encodeURI('https://api.newhorizons.uy/controller/checkout.php?r=' + r + '&e=' + e + '&a=' + a + '&d=' + d + '&extid=' + extid),
                    summary: {
                        productLabel: config.product.label,
                        product: config.product.price,
                        shipping: config.envio,
                        discountLabel: config.discount.label,
                        discount: config.discount.price,
                        charge: config.cargos,
                        taxes: config.taxes,
                        arrears: config.pendiente

                    }
                },

                render: {
                    container: config.render.pos,
                    label: config.render.label
                },

                theme: {
                    elementsColor: config.theme.elements,
                    headerColor: config.theme.header,
                }
            });

        } else {
            console.error('NO HAY AMOUNT')
        }

    }
}
