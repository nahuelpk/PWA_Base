
var app = new flex();

var baseProps = { baseColor: 'teal-700', secondColor: 'teal-800' };

$('body').rView('nav', Object.assign({ title: 'flex.js' }, baseProps))


//let hero = $('body').comp('hero', Object.assign({ title: '<span class="block">Level Up Your <span class="block mt-1 text-{{baseColor}} lg:inline lg:mt-0">Web Applications</span></span>', onc: `console.log('${baseProps.baseColor}')` }, baseProps));





window.addEventListener('locationchange', function(e){
    e.preventDefault()
    console.log('location changed!', e);
})

history.pushState = ( f => function pushState(){
    var ret = f.apply(this, arguments);
    window.dispatchEvent(new Event('pushstate'));
    window.dispatchEvent(new Event('locationchange'));
    return ret;
})(history.pushState);

history.replaceState = ( f => function replaceState(){
    var ret = f.apply(this, arguments);
    window.dispatchEvent(new Event('replacestate'));
    window.dispatchEvent(new Event('locationchange'));
    return ret;
})(history.replaceState);

window.addEventListener('popstate',()=>{
    window.dispatchEvent(new Event('locationchange'))
});
