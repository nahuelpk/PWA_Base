 $('body').comp('nav', props).then(nav=>{
    let menu = [{title: 'Home', href: '#/home'}, {title: 'About', href: '#/about'}, {title: 'Contact', href: '#/contact'}];

    menu.forEach(item=>{
    $(nav).find('nav').comp('nav-link', Object.assign({ text: item.title, href: item.href }, baseProps))

    })

    return nav
});